﻿using System;
using System.Threading.Tasks;
using Visma.Data.Core;
using Visma.Data.Data;
using Visma.Domain;

namespace Visma.API.Infrastructure
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        private readonly DataContext _context;

        public Repository(DataContext context)
        {
            _context = context;
        }

        public void Add(TEntity entity)
        {
            _context.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _context.Remove(entity);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<TEntity> GetAsync(Guid id)
        {
            return await _context.FindAsync<TEntity>(id);
        }
    }
}
