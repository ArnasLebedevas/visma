﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Visma.Data.Core.Employees;
using Visma.Data.Core.Employees.Params;
using Visma.Domain.Employee;

namespace Visma.API.Controllers
{
    public class EmployeesController : BaseApiController
    {
        [HttpGet]
        public async Task<IActionResult> GetEmployees([FromQuery] EmployeeParams param)
        {
            return HandlePagedResult(await Mediator.Send(new EmployeesList.Query { Params = param}));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployee(Guid id)
        {
            return HandleResult(await Mediator.Send(new EmployeeDetails.Query { ID = id }));
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee(Employee employee)
        {
            return HandleResult(await Mediator.Send(new CreateEmployee.Command { Employee = employee }));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditEmployee(Guid id, Employee employee)
        {
            employee.ID = id;
            return HandleResult(await Mediator.Send(new EditEmployee.Command { Employee = employee }));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            return HandleResult(await Mediator.Send(new DeleteEmployee.Command { ID = id }));
        }
    }
}
