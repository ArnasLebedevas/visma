﻿import { makeAutoObservable, reaction, runInAction } from "mobx";
import { Employee, EmployeeFormValues } from '../models/employee';
import { format } from 'date-fns';
import agent from "../api/agent";
import { Pagination, PagingParams } from "../models/pagination";

export default class EmployeeStore {
    employeeRegistry = new Map<string, Employee>();
    selectedEmployee: Employee | undefined = undefined;
    loading = false;
    loadingInitial = false;
    pagination: Pagination | null = null;
    pagingParams = new PagingParams();
    predicate = new Map().set('all', true)

    constructor() {
        makeAutoObservable(this);

        reaction(
            () => this.predicate.keys(),
            () => {
                this.pagingParams = new PagingParams();
                this.employeeRegistry.clear();
                this.loadEmployees();
            }
        )
    }

    setPagingParams = (pagingParams: PagingParams) => {
        this.pagingParams = pagingParams;
    }

    setPredicate = (predicate: string, value: string | Date) => {
        const resetPredicate = () => {
            this.predicate.forEach((value, key) => {
                if (key !== 'startDate') this.predicate.delete(key);
            })
        }
        switch (predicate) {
            case 'all':
                resetPredicate();
                this.predicate.set('all', true);
                break;
            case 'firstName':
                resetPredicate();
                this.predicate.set('firstName', value);
                break;
            case 'startDate':
                this.predicate.delete('startDate');
                this.predicate.set('startDate', value);
        }
    }

    get axiosParams() {
        const params = new URLSearchParams();
        params.append('pageNumber', this.pagingParams.pageNumber.toString());
        params.append('pageSize', this.pagingParams.pageSize.toString());
        this.predicate.forEach((value, key) => {
            if (key === 'startDate') {
                params.append(key, (value as Date).toISOString())
            } else {
                params.append(key, value);
            }
        })
        return params;
    }

    get employeesByDate() {
        return Array.from(this.employeeRegistry.values()).sort((a, b) => a.birthdate!.getTime() - b.birthdate!.getTime());
    }

    get groupedEmployees() {
        return Object.entries(
            this.employeesByDate.reduce((employees, employee) => {
                const birthdate = format(employee.birthdate!, 'dd MMM yyyy')
                employees[birthdate] = employees[birthdate] ? [...employees[birthdate], employee] : [employee];
                return employees;
            }, {} as { [key: string]: Employee[] })
        )
    }

    loadEmployees = async () => {
        this.loadingInitial = true;
        try {
            const result = await agent.Employees.list(this.axiosParams);
            result.data.forEach(employee => {
                this.setEmployee(employee);
            })
            this.setPagination(result.pagination);
            this.setLoadingInitial(false);
        } catch (error) {
            console.log(error);
            this.setLoadingInitial(false);
        }
    }

    setPagination = (pagination: Pagination) => {
        this.pagination = pagination;
    }

    loadEmployee = async (id: string) => {
        let employee = this.getEmployee(id);
        if (employee) {
            this.selectedEmployee = employee;
            return employee;
        } else {
            this.loadingInitial = true;
            try {
                employee = await agent.Employees.details(id);
                this.setEmployee(employee);
                runInAction(() => {
                    this.selectedEmployee = employee;
                })
                this.setLoadingInitial(false);
                return employee;
            } catch (error) {
                console.log(error);
                this.setLoadingInitial(false);
            }
        }
    }

    private setEmployee = (employee: Employee) => {
        employee.birthdate = new Date(employee.birthdate!);
        this.employeeRegistry.set(employee.id, employee);
    }

    private getEmployee = (id: string) => {
        return this.employeeRegistry.get(id);
    }

    setLoadingInitial = (state: boolean) => {
        this.loadingInitial = state;
    }

    createEmployee = async (employee: EmployeeFormValues) => {
        this.loading = true;
        try {
            await agent.Employees.create(employee);
            const newEmployee = new Employee(employee);
            this.setEmployee(newEmployee);
            runInAction(() => {
                this.selectedEmployee = newEmployee;
                this.loading = false;
            })
        } catch (error) {
            console.log(error);
        }
    }

    updateEmployee = async (employee: EmployeeFormValues) => {
        this.loading = true;
        try {
            await agent.Employees.update(employee);
            runInAction(() => {
                if (employee.id) {
                    let updatedEmployee = { ...this.getEmployee(employee.id), ...employee }
                    this.employeeRegistry.set(employee.id, updatedEmployee as Employee);
                    this.selectedEmployee = updatedEmployee as Employee;
                    this.loading = false;
                }
            })
        } catch (error) {
            console.log(error);
        }
    }

    deleteEmployee = async (id: string) => {
        this.loading = true;
        try {
            await agent.Employees.delete(id);
            runInAction(() => {
                this.employeeRegistry.delete(id);
                this.loading = false;
            })
        } catch (error) {
            runInAction(() => {
                this.loading = false;
                console.log(error);
            })
        }
    }
}