import React, { useEffect } from 'react';
import { Container } from 'semantic-ui-react';
import NavBar from './NavBar';
import EmployeeDashboard from '../../features/employees/dashboard/EmployeeDashboard';
import { observer } from 'mobx-react-lite';
import { Route, Switch, useLocation } from 'react-router';
import HomePage from '../../features/home/HomePage';
import EmployeeForm from '../../features/employees/form/EmployeeForm';
import EmployeeDetails from '../../features/employees/details/EmployeeDetails';
import TestErrors from '../../features/errors/TestError';
import { ToastContainer } from 'react-toastify';
import NotFound from '../../features/errors/NotFound';
import ServerError from '../../features/errors/ServerError';
import { useStore } from '../stores/store';
import LoadingComponent from './LoadingComponent';
import ModalContainer from '../common/modals/ModalContainer';
import PrivateRoute from './PrivateRoute';
import EmployeeFormUpdate from '../../features/employees/form/EmployeeFormUpdate';

function App() {
    const location = useLocation();
    const { commonStore, userStore } = useStore();

    useEffect(() => {
        if (commonStore.token) {
            userStore.getUser().finally(() => commonStore.setAppLoaded());
        } else {
            commonStore.setAppLoaded();
        }
    }, [commonStore, userStore])

    if (!commonStore.appLoaded) return <LoadingComponent content='Loading app...' />

    return (
        <>
            <ToastContainer position='bottom-right' hideProgressBar />
            <ModalContainer />
            <Route exact path='/' component={HomePage} />
            <Route
                path={'/(.+)'}
                render={() => (
                    <>
                        <NavBar />
                        <Container style={{ marginTop: '7em' }}>
                            <Switch>
                                <PrivateRoute exact path='/employees' component={EmployeeDashboard} />
                                <PrivateRoute path='/employees/:id' component={EmployeeDetails} />
                                <PrivateRoute key={location.key} path='/manage/:id' component={EmployeeFormUpdate} />
                                <PrivateRoute key={location.key} path='/createEmployee' component={EmployeeForm} />
                                <PrivateRoute path='/errors' component={TestErrors} />
                                <Route path='/server-error' component={ServerError} />
                                <Route component={NotFound} />
                            </Switch>
                        </Container>
                    </>
                )}
            />
        </>
    );
}

export default observer(App);
