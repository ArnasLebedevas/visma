﻿export interface Employee {
    id: string;
    firstName: string;
    lastName: string;
    birthdate: Date | null;
    employmentDate: Date | null;
    boss: string;
    homeAdress: string;
    role: string;
    currentSalary: string;
}

export class Employee implements Employee {
    constructor(init?: EmployeeFormValues) {
        Object.assign(this, init);
    }
}

export class EmployeeFormValues {
    id?: string = undefined;
    firstName: string = '';
    lastName: string = '';
    birthdate: Date | null = null;
    employmentDate: Date | null = null;
    boss: string = '';
    homeAdress: string = '';
    role: string = '';
    currentSalary: string = '';

    constructor(employee?: EmployeeFormValues) {
        if (employee) {
            this.id = employee.id;
            this.firstName = employee.firstName;
            this.lastName = employee.lastName;
            this.birthdate = employee.birthdate;
            this.employmentDate = employee.employmentDate;
            this.boss = employee.boss;
            this.homeAdress = employee.homeAdress;
            this.role = employee.role;
            this.currentSalary = employee.currentSalary;
        }
    }
}