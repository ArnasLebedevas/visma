﻿import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { Button, Header, Segment } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';
import { Formik, Form} from 'formik';
import * as Yup from 'yup';
import MyTextInput from '../../../app/common/form/MyTextInput';
import MySelectInput from '../../../app/common/form/MySelectInput';
import { roleOptions } from '../../../app/common/options/roleOptions';
import MyDateInput from '../../../app/common/form/MyDateInput';
import { EmployeeFormValues } from '../../../app/models/employee';
import moment from 'moment';
import { v4 as uuid } from 'uuid';

export default observer(function EmployeeForm() {
    const history = useHistory();
    const { employeeStore } = useStore();
    const { createEmployee, loading, loadEmployee, loadingInitial } = employeeStore;
    const { id } = useParams<{ id: string }>();
    const [employee, setEmployee] = useState<EmployeeFormValues>(new EmployeeFormValues());

    const validationSchema = Yup.object({
        firstName: Yup.string().required('First name is required').max(50).notOneOf([Yup.ref('lastName')], 'First name cannot be equal to last name'),
        lastName: Yup.string().required('Last name is required').max(50),
        homeAdress: Yup.string().required('Home adress is required'),
        role: Yup.string().required(),
        birthdate: Yup.string().required('Birthdate is required').nullable().test("DOB", "Employee must be 18 years old", value => { return moment().diff(moment(value), 'years') >= 18;}
        ).test("DOB", "Employee cannot be older than 70 years old", value => { return moment().diff(moment(value), 'years') <= 70; }),
        employmentDate: Yup.date().required('Employment date is required').nullable().min(new Date('2000-01-01'), `Employment date cannot be earlier than ${'2000-01-01'}`)
            .max(new Date(), `Employment date cannot be future`),
        currentSalary: Yup.number().required('Current salary required').test('Is positive?', 'The current salary must be greater than 0', (value) => value! >= 0),
        boss: Yup.string().when('role', { is: 'ceo', then: Yup.string().notRequired(), otherwise: Yup.string().required() })
    })

    useEffect(() => {
        if (id) loadEmployee(id).then(employee => setEmployee(new EmployeeFormValues(employee)))
    }, [id, loadEmployee])

    function handleFormSubmit(employee: EmployeeFormValues) {
        if (!employee.id) {
            let newEmployee = {
                ...employee,
                id: uuid()
            };
            createEmployee(newEmployee).then(() => history.push(`/employees/${newEmployee.id}`))
        }
    }

    if (loadingInitial) return <LoadingComponent content='Loading employee...' />

    return (
        <Segment clearing>
            <Header content='Employee Details' sub color='teal' />
            <Formik
                validationSchema={validationSchema}
                enableReinitialize
                initialValues={employee}
                onSubmit={values => handleFormSubmit(values)}
            >
                {({ handleSubmit, isValid, isSubmitting, dirty }) => (
                    <Form className='ui form' onSubmit={handleSubmit} autoComplete='off'>
                        <MyTextInput placeholder='First Name' name='firstName' />
                        <MyTextInput placeholder='Last Name' name='lastName'/>
                        <MyTextInput placeholder='Home Adress' name='homeAdress' />
                        <MyDateInput placeholderText='Birthdate' name='birthdate' showMonthDropdown showYearDropdown  timeCaption='time' dateFormat='dd MMM yyyy' />
                        <MyDateInput placeholderText='Employment Date' name='employmentDate' showMonthDropdown showYearDropdown timeCaption='time' dateFormat='dd MMM yyyy' />
                        <MySelectInput options={roleOptions} placeholder='Role' name='role' />
                        <MyTextInput placeholder='Current Salary' name='currentSalary' />
                        <MyTextInput placeholder='Boss' name='boss' />
                        <Button
                            disabled={isSubmitting || !dirty || !isValid}
                            loading={loading}
                            floated='right'
                            positive type='submit'
                            content='Submit'
                        />
                        <Button as={Link} to='/employees' floated='right' type='button' content='Cancel' />
                    </Form>
                    )}
            </Formik>
        </Segment>
    )
})