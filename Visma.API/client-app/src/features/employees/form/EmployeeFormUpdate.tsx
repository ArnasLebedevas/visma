﻿import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import { Button, Header, Segment } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import MyTextInput from '../../../app/common/form/MyTextInput';
import { EmployeeFormValues } from '../../../app/models/employee';

export default observer(function EmployeeFormUpdate() {
    const history = useHistory();
    const { employeeStore } = useStore();
    const { updateEmployee, loading, loadEmployee, loadingInitial } = employeeStore;
    const { id } = useParams<{ id: string }>();
    const [employee, setEmployee] = useState<EmployeeFormValues>(new EmployeeFormValues());

    const validationSchema = Yup.object({
        currentSalary: Yup.number().required('Current salary required').test('Is positive?', 'The current salary must be greater or equal to 0', (value) => value! >= 0)
    })

    useEffect(() => {
        if (id) loadEmployee(id).then(employee => setEmployee(new EmployeeFormValues(employee)))
    }, [id, loadEmployee])

    function handleFormSubmit(employee: EmployeeFormValues) {
        if (employee.id) {
            updateEmployee(employee).then(() => history.push(`/employees/${employee.id}`))
        }
    }

    if (loadingInitial) return <LoadingComponent content='Loading employee...' />

    return (
        <Segment clearing>
            <Header content='Employee Salary' sub color='teal' />
            <Formik
                validationSchema={validationSchema}
                enableReinitialize
                initialValues={employee}
                onSubmit={values => handleFormSubmit(values)}
            >
                {({ handleSubmit, isValid, isSubmitting, dirty }) => (
                    <Form className='ui form' onSubmit={handleSubmit} autoComplete='off'>
                        <MyTextInput placeholder='Current Salary' name='currentSalary' />
                        <Button
                            disabled={isSubmitting || !dirty || !isValid}
                            loading={loading}
                            floated='right'
                            positive type='submit'
                            content='Submit'
                        />
                        <Button as={Link} to='/employees' floated='right' type='button' content='Cancel' />
                    </Form>
                )}
            </Formik>
        </Segment>
    )
})