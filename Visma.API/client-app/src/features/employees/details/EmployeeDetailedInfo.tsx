﻿import { observer } from 'mobx-react-lite';
import React from 'react'
import { format } from 'date-fns';
import { Segment, Grid, Icon } from 'semantic-ui-react'
import { Employee } from '../../../app/models/employee';

interface Props {
    employee: Employee
}

export default observer(function EmployeeDetailedInfo({ employee }: Props) {
    return (
        <Segment.Group>
            <Segment attached='top'>
                <Grid>
                    <Grid.Column width={1}>
                        <Icon size='large' color='teal' name='info' />
                    </Grid.Column>
                    <Grid.Column width={15}>
                        <p>{employee.role}</p>
                    </Grid.Column>
                </Grid>
            </Segment>
            <Segment attached>
                <Grid verticalAlign='middle'>
                    <Grid.Column width={1}>
                        <Icon name='birthday' size='large' color='teal' />
                    </Grid.Column>
                    <Grid.Column width={15}>
                        <span>
                            {format(employee.birthdate!, 'dd MMM yyyy')}
                        </span>
                    </Grid.Column>
                </Grid>
            </Segment>
            <Segment attached>
                <Grid verticalAlign='middle'>
                    <Grid.Column width={1}>
                        <Icon name='location arrow' size='large' color='teal' />
                    </Grid.Column>
                    <Grid.Column width={11}>
                        <span>{employee.homeAdress}</span>
                    </Grid.Column>
                </Grid>
            </Segment>
        </Segment.Group>
    )
})