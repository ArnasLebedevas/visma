﻿import { observer } from 'mobx-react-lite';
import React from 'react'
import { Link } from 'react-router-dom';
import Moment from 'moment';
import { Button, Header, Item, Segment, Image } from 'semantic-ui-react'
import { Employee } from '../../../app/models/employee';

const employeeImageStyle = {
    filter: 'brightness(30%)'
};

const employeeImageTextStyle = {
    position: 'absolute',
    bottom: '5%',
    left: '5%',
    width: '100%',
    height: 'auto',
    color: 'white'
};

interface Props {
    employee: Employee
}

export default observer(function EmployeeDetailedHeader({ employee }: Props) {
    return (
        <Segment.Group>
            <Segment basic attached='top' style={{ padding: '0' }}>
                <Image src={`/assets/roleImages/${employee.role}.jpg`} fluid style={employeeImageStyle} />
                <Segment style={employeeImageTextStyle} basic>
                    <Item.Group>
                        <Item>
                            <Item.Content>
                                <Header
                                    size='huge'
                                    content={employee.firstName + ' ' + employee.lastName}
                                    style={{ color: 'white' }}
                                />
                                <p>Employment Date: {Moment(employee.employmentDate).format('dd MMM yyyy')}</p>
                                <p>Employee Boss: <strong>{employee.boss || "Doesn't exist"}</strong></p>
                                <p>Salary: <strong>{employee.currentSalary + ' €'}</strong></p>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </Segment>
            </Segment>
            <Segment clearing attached='bottom'>
                <Button as={Link} to={`/manage/${employee.id}`} color='orange' floated='right'>
                    Update Salary
                </Button>
            </Segment>
        </Segment.Group>
    )
})
