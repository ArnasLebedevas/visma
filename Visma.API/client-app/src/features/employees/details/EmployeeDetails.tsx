﻿import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { Grid } from 'semantic-ui-react';
import LoadingComponent from '../../../app/layout/LoadingComponent';
import { useStore } from '../../../app/stores/store';
import EmployeeDetailedHeader from './EmployeeDetailedHeader';
import EmployeeDetailedInfo from './EmployeeDetailedInfo';
import EmployeeDetailedSidebar from './EmployeeDetailedSidebar';

export default observer(function EmployeeDetails() {

    const { employeeStore } = useStore();
    const { selectedEmployee: employee, loadEmployee, loadingInitial } = employeeStore;
    const { id } = useParams<{ id: string }>();

    useEffect(() => {
        if (id) loadEmployee(id);
    }, [id, loadEmployee]);

    if (loadingInitial || !employee) return <LoadingComponent />;

    return (
        <Grid>
            <Grid.Column width={10}>
                <EmployeeDetailedHeader employee={employee} />
                <EmployeeDetailedInfo employee={employee} />
            </Grid.Column>
            {employee.role === "boss" &&
                <Grid.Column width={6}>
                <EmployeeDetailedSidebar />
                </Grid.Column>}
        </Grid>
    )
})