﻿import { observer } from 'mobx-react-lite';
import React from 'react';
import { Menu } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';

export default observer(function EmployeeList() {

    const { employeeStore } = useStore();
    const { employeesByDate } = employeeStore;

    return (
        <>
            <Menu vertical  size='large' style={{ width: '100%' }}>
                <Menu.Item 
                    content={'Employee Count: ' + employeesByDate.length}
                />
            </Menu>
        </>
    )
})