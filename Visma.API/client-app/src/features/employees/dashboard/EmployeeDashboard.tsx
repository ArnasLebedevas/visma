﻿import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { Grid, Loader } from 'semantic-ui-react';
import { PagingParams } from '../../../app/models/pagination';
import { useStore } from '../../../app/stores/store';
import EmployeeFilters from './EmployeeFilters';
import EmployeeInfo from './EmployeeInfo';
import EmployeeList from './EmployeeList';
import EmployeeListItemPlaceholder from './EmployeeListItemPlaceholder';

export default observer(function EmployeeDashboard() {
    const { employeeStore } = useStore();
    const { loadEmployees, employeeRegistry, setPagingParams, pagination } = employeeStore;
    const [loadingNext, setLoadingNext] = useState(false);

    function handleGetNext() {
        setLoadingNext(true);
        setPagingParams(new PagingParams(pagination!.currentPage + 1))
        loadEmployees().then(() => setLoadingNext(false));
    }

    useEffect(() => {
        if (employeeRegistry.size <= 1) loadEmployees();
    }, [employeeRegistry.size, loadEmployees])

    return (
        <Grid>
            <Grid.Column width='10'>
                {employeeStore.loadingInitial && !loadingNext ? (
                    <>
                        <EmployeeListItemPlaceholder />
                        <EmployeeListItemPlaceholder />
                    </>
                ) : (
                    <InfiniteScroll
                        pageStart={0}
                        loadMore={handleGetNext}
                        hasMore={!loadingNext && !!pagination && pagination.currentPage < pagination.totalPages}
                        initialLoad={false}
                    >
                        <EmployeeList />
                    </InfiniteScroll>
                )}
            </Grid.Column>
            <Grid.Column width='6'>
                <EmployeeFilters />
                <EmployeeInfo />
            </Grid.Column>
            <Grid.Column width='10'>
                <Loader active={loadingNext} />
            </Grid.Column>
        </Grid>
    )
})