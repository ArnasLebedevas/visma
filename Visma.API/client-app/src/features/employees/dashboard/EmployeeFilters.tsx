﻿import { observer } from 'mobx-react-lite';
import React from 'react';
import Calendar from 'react-calendar';
import { Header, Input, Menu } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';

export default observer(function EmployeeFilters() {
    const { employeeStore: { predicate, setPredicate } } = useStore();

    return (
        <>
            <Menu vertical size='large' style={{ width: '100%' }}>
                <Header icon='filter' attached color='teal' content='Filters' />
                <Menu.Item
                    content='All Employees'
                    onClick={() => setPredicate('all', 'true')}
                />
                <Menu.Item>
                    <Input
                        icon='search'
                        placeholder='Search name...'
                        onChange={() => setPredicate('firstName', 'true')}
                    />
                </Menu.Item>
            </Menu>
            <Header />
            <Calendar onChange={(date) => setPredicate('startDate', date as Date)} value={predicate.get('startDate') || new Date()} />
        </>
    )
})