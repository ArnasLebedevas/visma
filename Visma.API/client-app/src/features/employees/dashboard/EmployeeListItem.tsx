﻿import {format} from 'date-fns';
import React, { SyntheticEvent, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon, Item, Segment } from 'semantic-ui-react';
import { Employee } from '../../../app/models/employee';
import { useStore } from '../../../app/stores/store';

interface Props {
    employee: Employee;
}

export default function EmployeeListItem({ employee }: Props) {
    const { employeeStore } = useStore();
    const { deleteEmployee, loading } = employeeStore;

    const [target, setTarget] = useState('');

    function handleEmployeeDelete(e: SyntheticEvent<HTMLButtonElement>, id: string) {
        setTarget(e.currentTarget.name);
        deleteEmployee(id);
    }

    return (
        <Segment.Group>
            <Segment>
                <Item.Group>
                    <Item>
                        <Item.Image size='tiny' circular src='/assets/user.png' />
                        <Item.Content>
                            <Item.Header as={Link} to={`/employees/${employee.id}`}>{employee.firstName} {employee.lastName}</Item.Header>
                            <Item.Description>{employee.role}</Item.Description>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Segment>
            <Segment>
                <span>
                    <Icon name='birthday' /> {format(employee.birthdate!, 'dd MMM yyyy')}
                    <Icon name={'marker'} /> {employee.homeAdress}
                </span>
            </Segment>
            <Segment secondary>
                Boss: {employee.boss || ""}
            </Segment>
            <Segment clearing>
                <span>{employee.currentSalary + '€'}</span>
                <Button
                    as={Link}
                    to={`/employees/${employee.id}`}
                    color='teal'
                    floated='right'
                    content='View'
                />
                <Button
                    name={employee.id}
                    loading={loading && target === employee.id}
                    onClick={(e) => handleEmployeeDelete(e, employee.id)}
                    floated='right'
                    content='Delete'
                    color='red'
                />
            </Segment>
        </Segment.Group>
    )
}