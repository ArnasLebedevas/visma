﻿import { observer } from 'mobx-react-lite';
import { Fragment } from 'react';
import { Header } from 'semantic-ui-react';
import { useStore } from '../../../app/stores/store';
import EmployeeListItem from './EmployeeListItem';

export default observer(function EmployeeList() {

    const { employeeStore } = useStore();
    const { groupedEmployees } = employeeStore;

    return (
        <>
            {groupedEmployees.map(([group, employees]) => (
                <Fragment key={group}>
                    <Header sub color='black'>
                        {employees.map(employee => (
                            <EmployeeListItem key={employee.id} employee={employee} />
                        ))}
                    </Header>
                </Fragment>
            ))}
        
        </>
    )
})