﻿using System;

namespace Visma.Domain.Employee
{
    public class Employee : IEntity
    {
        public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthdate { get; set; }
        public DateTime EmploymentDate { get; set; }
        public string Boss { get; set; }
        public string HomeAdress { get; set; }
        public string Role { get; set; }
        public decimal CurrentSalary { get; set; }
    }
}
