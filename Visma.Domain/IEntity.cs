﻿using System;

namespace Visma.Domain
{
    public interface IEntity
    {
        public Guid ID { get; set; }
    }
}
