﻿using System;
using System.Threading.Tasks;

namespace Visma.Data.Core
{
    public interface IRepository<TEntity>
    {
        void Add(TEntity entity);
        void Remove(TEntity entity);
        Task<bool> SaveAllAsync();
        Task<TEntity> GetAsync(Guid id);
    }
}
