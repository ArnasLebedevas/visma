﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Visma.Data.Core.Result;
using Visma.Domain.Employee;

namespace Visma.Data.Core.Employees
{
    public class DeleteEmployee
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Guid ID { get; set; }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IRepository<Employee> _repository;

            public Handler(IRepository<Employee> repository)
            {
                _repository = repository;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var employee = await _repository.GetAsync(request.ID);

                _repository.Remove(employee);

                var result = await _repository.SaveAllAsync();

                if (!result) return Result<Unit>.Failure("Failed to delete the employee");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}
