﻿using AutoMapper;
using Visma.Domain.Employee;
using Visma.Models.Employee;

namespace Visma.Data.Core.Employees.Mapper
{
    public class EmployeeMapper : Profile
    {
        public EmployeeMapper()
        {
            CreateMap<Employee, Employee>();
            CreateMap<Employee, EmployeeModel>();
        }
    }
}
