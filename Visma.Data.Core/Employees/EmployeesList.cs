﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Visma.Data.Core.Employees.Params;
using Visma.Data.Core.PagedList;
using Visma.Data.Core.Result;
using Visma.Data.Data;
using Visma.Models.Employee;

namespace Visma.Data.Core.Employees
{
    public class EmployeesList
    {
        public class Query : IRequest<Result<PagedList<EmployeeModel>>>
        {
            public EmployeeParams Params { get; set; }

        }

        public class Handler : IRequestHandler<Query, Result<PagedList<EmployeeModel>>>
        {
            private readonly DataContext _context;
            private readonly IMapper _mapper;

            public Handler(DataContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Result<PagedList<EmployeeModel>>> Handle(Query request, CancellationToken cancellationToken)
            {
                var query = _context.Employees
                    .Where(d => d.Birthdate >= request.Params.StartDate)
                    .OrderBy(b => b.Birthdate)
                    .ProjectTo<EmployeeModel>(_mapper.ConfigurationProvider)
                    .AsQueryable();

                var filterByName = request.Params.FirstName;

                if(!string.IsNullOrEmpty(filterByName))
                {
                    query = query.Where(x => x.FirstName.Contains(filterByName));
                }

                return Result<PagedList<EmployeeModel>>.Success(
                    await PagedList<EmployeeModel>.CreateAsync(query, request.Params.PageNumber, request.Params.PageSize));
            }
        }
    }
}
