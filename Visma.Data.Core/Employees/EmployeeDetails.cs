﻿using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using Visma.Data.Core.Result;
using Visma.Domain.Employee;

namespace Visma.Data.Core.Employees
{
    public class EmployeeDetails
    {
        public class Query : IRequest<Result<Employee>>
        {
            public Guid ID { get; set; }
        }

        public class Handler : IRequestHandler<Query, Result<Employee>>
        {
            private readonly IRepository<Employee> _repository;

            public Handler(IRepository<Employee> repository)
            {
                _repository = repository;
            }

            public async Task<Result<Employee>> Handle(Query request, CancellationToken cancellationToken)
            {
                var employee = await _repository.GetAsync(request.ID);

                return Result<Employee>.Success(employee);
            }
        }
    }
}
