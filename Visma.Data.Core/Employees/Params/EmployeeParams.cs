﻿using System;
using Visma.Data.Core.PagedList;

namespace Visma.Data.Core.Employees.Params
{
    public class EmployeeParams : PagingParams
    {
        public string FirstName { get; set; }
        public DateTime StartDate { get; set; } = DateTime.UtcNow;
    }
}
