﻿using FluentValidation;
using Visma.Domain.Employee;

namespace Visma.Data.Core.Employees.Validator
{
    public class EmployeeValidator : AbstractValidator<Employee>
    {
        public EmployeeValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().MaximumLength(50).NotEqual(x => x.LastName);
            RuleFor(x => x.LastName).NotEmpty().MaximumLength(50);
            RuleFor(x => x.Birthdate).NotEmpty();
            RuleFor(x => x.HomeAdress).NotEmpty();
            RuleFor(x => x.CurrentSalary).NotEmpty().GreaterThanOrEqualTo(0);
            RuleFor(x => x.Role).NotEmpty();
            RuleFor(x => x.EmploymentDate).NotEmpty();
        }
    }
}
