﻿using AutoMapper;
using FluentValidation;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using Visma.Data.Core.Employees.Validator;
using Visma.Data.Core.Result;
using Visma.Domain.Employee;

namespace Visma.Data.Core.Employees
{
    public class EditEmployee
    {
        public class Command : IRequest<Result<Unit>>
        {
            public Employee Employee { get; set; }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(x => x.Employee).SetValidator(new EmployeeValidator());
            }
        }

        public class Handler : IRequestHandler<Command, Result<Unit>>
        {
            private readonly IRepository<Employee> _repository;
            private readonly IMapper _mapper;

            public Handler(IRepository<Employee> repository, IMapper mapper)
            {
                _repository = repository;
                _mapper = mapper;
            }

            public async Task<Result<Unit>> Handle(Command request, CancellationToken cancellationToken)
            {
                var employee = await _repository.GetAsync(request.Employee.ID);

                if (employee == null) return null;

                _mapper.Map(request.Employee, employee);

                var result = await _repository.SaveAllAsync();

                if (!result) return Result<Unit>.Failure("Failed to update employee");

                return Result<Unit>.Success(Unit.Value);
            }
        }
    }
}
